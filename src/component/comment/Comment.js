import React from 'react';
import Styles from '../../App.module.css'
const Comment = ({name,email,onClick}) => {
    return (
        <div onClick={onClick} className={Styles.comment}>
            <p>name : {name}</p>
            <p>email : {email}</p>
        </div>
    )
};
export default Comment