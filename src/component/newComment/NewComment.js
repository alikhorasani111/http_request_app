import React, {useState} from 'react'
import Styles from "../../App.module.css";
import axios from "axios";

const NewComment = ({setComments}) => {
    const [comment, setComment] = useState({
        name: "",
        email: "",
        content: ""
    });
    const changeHandler = (e) => {
        setComment({...comment, [e.target.name]: e.target.value})
    };

    const postCommentHandler = () => {
        axios.post('http://localhost:3001/comments',{...comment}).then(()=>{
            return axios.get('http://localhost:3001/comments').then((response)=>{
                setComments(response.data)
            })
        }).catch((error)=>{
            console.log(error)
        })
    };
    return (
        <div className={Styles.newComment}>
            <h1>Add a New Comment</h1>
            <div className={Styles.margin}>
                <label>name</label>
                <input onChange={changeHandler} name="name" value={comment.name} type="text"/>
            </div>
            <div className={Styles.margin}>
                <label>email</label>
                <input onChange={changeHandler} name="email" value={comment.email} type="text"/>
            </div>
            <div className={Styles.margin}>
                <label>body</label>
                <textarea name="content" value={comment.content} onChange={changeHandler} rows="3"></textarea>
            </div>
            <button onClick={postCommentHandler} className={Styles.btn_add}>Add New Comment</button>
        </div>
    )
};
export default NewComment;