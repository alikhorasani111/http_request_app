import Styles from "../../App.module.css";
import React, {useEffect, useState} from "react";
import axios from 'axios'

const FullComment = ({commentId}) => {
    const [comment, setComment] = useState([]);

    useEffect(() => {
        if (commentId) {
            axios.get(`http://localhost:3001/comments/${commentId}`).then((response) => {
                setComment(response.data)
            }).catch();
        }
    }, [commentId]);

    const deletedHandler = () => {
        axios.delete(`http://localhost:3001/comments/${commentId}`).then((response) => {
            setComment(response.data)
        }).catch()
    };

    if (!commentId) return <p>pleas select a comment.</p>;
    return (
        <div className={Styles.fullComment}>
            <p>{comment.name}</p>
            <p>{comment.email}</p>
            <p>{comment.body}</p>
            <button onClick={deletedHandler} className={Styles.deleted}>Deleted</button>
        </div>
    )
};
export default FullComment;