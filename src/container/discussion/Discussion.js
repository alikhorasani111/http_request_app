import React, {useEffect, useState} from 'react'
import Comment from '../../component/comment/Comment'
import FullComment from "../../component/fullComment/FullComment";
import NewComment from "../../component/newComment/NewComment";
import Styles from '../../App.module.css'
import axios from 'axios';

const Discussion = () => {
    const [comments, setComments] = useState(null);
    const [selectedId, setSelectedId] = useState(null);

    useEffect(() => {
        axios.get('http://localhost:3001/comments').then((response) => {
            console.log(response)
            setComments(response.data);
        }).catch((error) => {
            console.log(error)
        })
    }, []);

    const selectCommentHandler = (id) => {
        setSelectedId(id)
    };

    return (
        <main>
            <section className={Styles.d_flex}>
                {comments ? comments.map((c) => <Comment
                    onClick={() => selectCommentHandler(c.id)}
                    key={c.id}
                    name={c.name}
                    email={c.email}
                    body={c.body}/>
                ) : <p>loading...</p>}
            </section>

            <section>
                <FullComment commentId={selectedId}/>
            </section>


            <section>
                <NewComment setComments={setComments}/>
            </section>
        </main>
    )
};
export default Discussion;